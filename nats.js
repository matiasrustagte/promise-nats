const NATS = require('nats');
const shortid = require('shortid');

/**
 * @typedef {Object} Request
 */

/**
 * @typedef {Object} Response
 * @property {Object=} error
 * @property {Object=} body
 */

module.exports = class PromiseNats {
  /**
   * @param {Object} settings
   * @param {string=} settings.url
   * @param {string=} settings.host
   * @param {string|number=} settings.port
   * @param {string=} settings.user
   * @param {string=} settings.password
   * @param {Object=} options
   * @param {string=} options.name
   * @param {Function=} options.onError
   */
  constructor ({ url, host = 'localhost', port = 4222, user, password } = {}, { name, onError } = {}) {
    if (!url) {
      if (user && password) {
        user = encodeURIComponent(user);
        password = encodeURIComponent(password);
        url = `nats://${user}:${password}@${host}:${port}`;
      } else {
        url = `nats://${host}:${port}`;
      }
    }
    this.nats = NATS.connect({ url });
    this.name = name || shortid.generate();
    this.onError = onError;
  }

  /**
   * @param {string}
   * @param {Object} body
   * @return {Promise.<Response>}
   */
  request (event, body = {}) {
    return new Promise((resolve, reject) => {
      this.nats.requestOne(event, JSON.stringify(body), {}, 1000, res => {
        try {
          if (res.code === NATS.REQ_TIMEOUT)
            throw Error(`Request "${event}"" failed: timeout.`);

          res = JSON.parse(res);
          if (!res.error && !res.body)
            throw Error(`Request "${event}"" failed: bad format.`);

          if (res.error)
            return reject(Error(`Request "${event}"" failed: ${res.error}`));

          return resolve(res.body);
        } catch (e) {
          if (this.onError)
            this.onError(e);
          reject(e);
        }
      });
    });
  }

  /**
   * @param {string} event
   * @param {Function} handler
   */
  onRequest (event, handler) {
    this.nats.subscribe(event, { queue: this.nats.name }, async (req, eventReply) => {
      try {
        req = JSON.parse(req);
        const body = await handler(req);
        this.nats.publish(eventReply, JSON.stringify({ body }));
      } catch (e) {
        if (this.onError)
          this.onError(e);
        this.nats.publish(eventReply, JSON.stringify({ error: e.message || e }));
      }
    });
  }

  /**
   * @param {string} event
   * @param {Object} body
   */
  dispatch (event, body = {}) {
    this.nats.publish(event, JSON.stringify(body));
  }

  /**
   * @param {string} event
   * @param {Function} handler
   */
  onEvent (event, handler) {
    this.nats.subscribe(event, { queue: this.nats.name }, async msg => {
      try {
        msg = JSON.parse(msg);
        try {
          await handler(msg);
        } catch (e) {
          if (this.onError)
            this.onError(e);
          console.log(`Event handler error for a message "${event}"`, e);
        }
      } catch (e) {
        if (this.onError)
          this.onError(e);
        console.log(`Bad JSON in a message "${event}"`, e);
      }
    });
  }
};