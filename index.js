const NATS = require('nats');
const shortid = require('shortid');

/**
 * @typedef {Object} Request
 */

/**
 * @typedef {Object} Response
 * @property {Object=} error
 * @property {Object=} body
 */

module.exports = require('./nats.js');
module.exports.Mock = require('./mock.js');