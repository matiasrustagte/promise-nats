module.exports = class PromiseNatsMock {
  constructor (settings, { log } = {}) {
    this.log = !!log;
  }
  async request (endpoint, data, cb) {
    if (this.log)
      console.log(`Requested: ${endpoint}\nData: ${JSON.stringify(data)}\n`);
    cb({});
  }
  onRequest (event, body, cb) {
  }
  async dispatch (event, data) {
    if (this.log)
      console.log(`Dispatched: ${event}\nData: ${JSON.stringify(data)}\n`);
  }
  onEvent (event, body, cb) {
  }
};